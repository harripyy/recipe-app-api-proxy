#!/bin/sh

set -e
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/cond.d/default.conf
nginx -g 'demon off;'

